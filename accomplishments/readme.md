# Accomplishments

# Stuff I've Made

## Novu

### Blogs

* https://novu.co/blog/product-development-friction-notifications-experience/
* https://novu.co/blog/digest-notifications-best-practices-example/

### Twitter

We made Twitter polls about the adjacent tools developers are using. This worked especially well if you know those adjacent tool companies have a devrel team or are active on social to tap into their existing audience. Use a screenshot of the poll results to promote a link to the blog itself. Turn the blog post into a longform twitter thread.

* (Tweet with 8.8k views) https://x.com/novuhq/status/1773673962049884558
* (Longform Tweet Thread with 1.6k views) https://x.com/novuhq/status/1772367392552681477
* (Wrote this super quickly when TSwift dropped TTPS. Didn't perform as well as I'd hoped. If I did this today, I'd rewrite the initial tweet copy to something like, "When @twsift drops a surprise double album at 2am with an Instagram post, it gets over 5 million likes. If Instagram sent 5 million notifications as they came in, it would be the Worst Notification Experience ever. Here's what you could learn from IG's notification strategy:) https://x.com/novuhq/status/1781360236214821095

### LinkedIn

As with Twitter, LI was pushing polls really hard. Make polls related to the blog. Screenshot and link to the blog. But our highest performing LinkedIn posts? Take twitter threads and convert them into LinkedIn Carousels. Carousels get egregiously high engagement rates. 

* 75% CTR https://www.linkedin.com/feed/update/urn:li:activity:7178382726805270528/
* 70% CTR https://www.linkedin.com/feed/update/urn:li:activity:7231785029406760961/
* 43% CTR https://www.linkedin.com/feed/update/urn:li:activity:7189419116045254656/
* 37% CTR https://www.linkedin.com/feed/update/urn:li:activity:7187136331083321345/
* 29% CTR https://www.linkedin.com/feed/update/urn:li:activity:7191453099843108864/
* 27% CTR https://www.linkedin.com/feed/update/urn:li:activity:7199150110910406657/
* 23% CTR https://www.linkedin.com/feed/update/urn:li:activity:7181744431757242370/
* 12% CTR https://www.linkedin.com/feed/update/urn:li:activity:7178805483162521600/
* 10% CTR https://www.linkedin.com/feed/update/urn:li:activity:7196527732501975041/

### Video

Producer of all our livestreams. Made better Youtube thumbnails, improved video titles for searchability, added descriptions so it videos back to our website/docs. I would then post-produce the longform office hours videos into short form videos for our socials.

* https://www.youtube.com/watch?v=plIlVgyMQI8
* https://www.youtube.com/watch?v=fDXQHd4NVws
* https://www.youtube.com/watch?v=zpz3Q2Iox2k
* https://www.youtube.com/watch?v=8fpghRkVWBY
* https://www.youtube.com/watch?v=VBHierIbPHc
* https://www.youtube.com/watch?v=IJM2b2sE7_o


## env0 

### Blogs
Wrote 13 blogs accounting for 15% of site traffic

* https://www.env0.com/blog/terraform-remote-state-using-a-remote-backend
* https://www.env0.com/blog/implement-atlantis-style-terraform-and-terragrunt-workflows-in-env0
* https://www.env0.com/blog/how-to-integrate-azure-devops-with-env0
* https://www.env0.com/blog/audit-logs
* https://www.env0.com/blog/custom-rbac-roles
* https://www.env0.com/blog/cloudformation-support
* https://www.env0.com/blog/terragrunt-run-all
* https://www.env0.com/blog/env0-workflows-get-a-power-up
* https://www.env0.com/blog/ad-hoc-tasks
* https://www.env0.com/blog/managing-pulumi-iac-with-env0
* https://www.env0.com/blog/non-admin-api-keys
* https://www.env0.com/blog/terraform-plugins-2023
* https://www.env0.com/blog/devops-people-and-process

Filmed 12 Youtube videos, Produced/Edited 55 videos, 11k views accounting for 40% of Youtube views (all organic).
* Made Youtube shorts https://www.youtube.com/@envZero/shorts
* https://www.youtube.com/playlist?list=PL9HY2FNWkmyiwZEv9izcb0qQBM_C3HDad 

Grew Twitter followers 100%, including viral content with 200k views
* https://x.com/envZero/status/1609749175922753537
* https://x.com/envZero/status/1632151162538582017
* https://x.com/envZero/status/1619389451653849089
* https://x.com/envZero/status/1611045125722865677
* https://x.com/envZero/status/1608910737711288320
* https://x.com/envZero/status/1563202147545595904
* https://x.com/envZero/status/1564616722002370580

## GitLab

* https://www.linkedin.com/feed/update/urn:li:activity:6461642510925209600/?updateEntityUrn=urn%3Ali%3Afs_updateV2%3A%28urn%3Ali%3Aactivity%3A6461642510925209600%2CFEED_DETAIL%2CEMPTY%2CDEFAULT%2Cfalse%29