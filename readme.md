# About Me

I live in an off-grid tiny house. I also van life, having driven over 40,000 miles (stopped keeping track at some point), visited 37 states, and 10 National Parks.

## How to Reach Me

Fastest way to get ahold of me is to call my cell. Otherwise, here's where my work lives online.

* Phone - 971.302.3095
* Email - kevindamaso@gmail.com
* GitLab (My repository so you can see what I'm up to) - https://gitlab.com/project_kmac
* LinkedIn - https://www.linkedin.com/in/kdamaso
* TikTok - http://tiktok.com/@project_kmac
* Twitch (I sometimes Livestream my work) - https://www.twitch.tv/project_kmac
* Twitter - https://twitter.com/project_kmac
* Instagram - https://instagram.com/project_kmac 
* Reddit - https://reddit.com/user/project_kmac/


## Snippets

* Music. Zach Bryan. Heilung. Portishead. CloZee.
* Sports. I don't follow the big three, but am down for a game. I'll talk UFC/MMA with you any day, however.
* Film. Wong Kar-Wai. Jean Pierre Melville. The Northman. 3000 Years of Longing. Slow, slice of life films like "Tokyo Cowboy" or "The Station Agent"
* TV. The Wire. Intelligence (the Canadian Series). Vikings. Patriot. Northern Exposure. Talk to me about late 90's to early 00's anime.
* Books. Mostly non-fiction and business books these days. I recently spent 18 months deep in Goldratt and DevOps material. Outside of that? Bryan Magee's *Confessions of a Philosopher*, *Siddhartha* by Hesse, Ender's Game and Shadow. *Stealing the Network* was a fun read. 
* Comics. *Transmetropolitan* by Ellis. *Criminal* by Brubaker. *Lucifer* by Mike Carey. 

*****
